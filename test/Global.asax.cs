﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Routing;

namespace test
{
    public class WebApiApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            com.intelligis.core.Services.Service.StartVerifyCertificate();
            GlobalConfiguration.Configure(com.intelligis.core.WebApiConfig.Register);
        }
    }
}
