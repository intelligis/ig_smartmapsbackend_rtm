﻿using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

// La información general sobre un ensamblado se controla mediante el siguiente 
// conjunto de atributos. Cambie los valores de estos atributos para modificar la información
// asociada a un ensamblado.
[assembly: AssemblyTitle("Intelligis Technologies")]
[assembly: AssemblyDescription("")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("GreenLight Telematics S.A.")]
[assembly: AssemblyProduct("IG_SmartMapsCore3")]
[assembly: AssemblyCopyright("(C) Copyright 2020, GreenLight Telematics S.A.  Todos los derechos reservados")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]

// Si ComVisible se establece en false, los componentes COM no verán los 
// tipos de este ensamblado. Si necesita obtener acceso a un tipo de este ensamblado desde 
// COM, establezca el atributo ComVisible en true en este tipo.
[assembly: ComVisible(false)]

// El siguiente GUID es para el Id. typelib cuando este proyecto esté expuesto a COM
[assembly: Guid("d80d84fd-7810-45c4-93d8-c150721978df")]

// La información de versión de un ensamblado consta de los siguientes cuatro valores:
//
//      Versión principal
//      Versión secundaria 
//      Número de compilación
//      Revisión
//
// Puede especificar todos los valores o usar los valores predeterminados de número de compilación y de revisión 
// mediante el carácter '*', como se muestra a continuación:
[assembly: AssemblyVersion("3.0.2.7")]
[assembly: AssemblyFileVersion("3.0.2.7")]
