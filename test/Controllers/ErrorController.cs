﻿//(C) Copyright 2020, GreenLight Telematics S.A.Todos los derechos reservados
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace com.intelligis.core.Controllers
{
    public class ErrorController : ApiController
    {
        [HttpGet, HttpPost, HttpPut, HttpDelete, HttpHead, HttpOptions, HttpPatch, AcceptVerbs(new string[] { "COPY", "LINK", "UNLINK", "PURGE", "LOCK", "UNLOCK", "PROPFIND", "VIEW" })]
        public HttpResponseMessage Handle404()
        {
            var response = Request.CreateResponse(HttpStatusCode.OK, new { IsSuccess = 0, Access = 1 });

            if (Request.Headers.Authorization != null)
                response.Headers.Add("Authorization", "Bearer " + Request.Headers.Authorization?.Parameter);
                //response.Headers.Add("Authorization", Request.Headers.Authorization?.Parameter);

            return response;
            //return Ok(new { IsSuccess = 0, Access = 1 });
        }
    }
}
