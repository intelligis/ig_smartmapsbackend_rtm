﻿using Excel;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Diagnostics;
using System.Drawing;
//(C) Copyright 2020, GreenLight Telematics S.A.Todos los derechos reservados

using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using com.intelligis.core.Services;
using System.Net.Http.Headers;

namespace com.intelligis.core.Controllers
{
    public class IG_SmartMapsController : ApiController
    {

       /*
        * Description:  Validaciones de sesión hacia Portal
        * 
        */
       [HttpPost]
        [Route("api/IG_smartMaps/IG_SmartMapsLoginPortal")]
        public HttpResponseMessage IG_SmartMapsLoginPortal([FromBody]Models.RequestPortalLogin request)
        {
            DataAccess.InstanceService dataAccess;
            HttpResponseMessage response;
            JObject jsonResponse;
            DataSet respuesta;
            object responseImage;
            Models.Response<int> responseUpload;
            string jsonString = string.Empty;
            Models.Request requestRTM = null;
            string esriTOken = string.Empty;
            try
            {
                requestRTM = new Models.Request();


                if (string.IsNullOrEmpty(Service.IsVerified))
                {
                    Service.StartVerifyCertificate();

                    //Petición no valida
                    response = Request.CreateResponse(HttpStatusCode.OK, new { IsSuccess = 0, Result = "Problemas con el certificado", Access = 1 });

                    if (Request.Headers.Authorization != null)
                        response.Headers.Add("Authorization", "Bearer " + Request.Headers.Authorization?.Parameter);

                    return response;
                }

                if (!ModelState.IsValid)
                {
                    //Petición no valida
                    response = Request.CreateResponse(HttpStatusCode.OK, new { IsSuccess = 0, Access = 1 });

                    if (Request.Headers.Authorization != null)
                        response.Headers.Add("Authorization", "Bearer " + Request.Headers.Authorization?.Parameter);

                    return response;
                }


                if (Request.Headers.Authorization==null || string.IsNullOrEmpty(Request.Headers.Authorization?.Parameter))
                {                 
                    //Petición no valida
                    response = Request.CreateResponse(HttpStatusCode.OK, new { IsSuccess = 0, Access = 1 });

                    if (Request.Headers.Authorization != null)
                        response.Headers.Add("Authorization", "Bearer " + Request.Headers.Authorization?.Parameter);

                    return response;
                }

                    esriTOken = Request.Headers.Authorization?.Parameter;
                    requestRTM.v2 = esriTOken;
                    string urlPortal = ConfigurationManager.AppSettings["urlPortal"];
                    Services.RestClient rest = new RestClient();
                    rest.endPoint = $"{ urlPortal }/sharing/rest/portals/self?f=json&token={esriTOken}";
                    rest.contentType = "application/Json";
                    rest.httpMethod = httpVerb.POST;
                    string result = rest.makeRequest();

                    JObject valueResponse = JObject.Parse(result);

                    JToken jGroups = null;

                    JToken jUser = valueResponse.SelectToken("user");

                    if (jUser.HasValues == false || jUser["username"] == null || string.IsNullOrEmpty(jUser.Value<string>("username")) || string.IsNullOrEmpty(jUser.Value<string>("fullName")))
                    {
                        //Petición no valida
                        response = Request.CreateResponse(HttpStatusCode.OK, new { IsSuccess = 0, Access = 1 });

                        if (Request.Headers.Authorization != null)
                            response.Headers.Add("Authorization", "Bearer " + Request.Headers.Authorization?.Parameter);

                        return response;
                    }


                    requestRTM.v1 = "GetAuthentication";
                    requestRTM.v3 = jUser.Value<string>("username");
                    requestRTM.v4 = jUser.Value<string>("fullName");
                    requestRTM.v5 = jUser.Value<string>("email");

                    rest = new RestClient();
                    rest.endPoint = $"{ urlPortal }/sharing/rest/community/users/{jUser.Value<string>("username")}/?f=json&token={ esriTOken }";
                    rest.contentType = "application/Json";
                    rest.httpMethod = httpVerb.POST;
                    result = rest.makeRequest();

                    valueResponse = JObject.Parse(result);

                    jGroups = valueResponse.SelectToken("groups");

                    if (jGroups.HasValues == false)
                    {
                        //Petición no valida
                        response = Request.CreateResponse(HttpStatusCode.OK, new { IsSuccess = 0, Access = 1 });

                        if (Request.Headers.Authorization != null)
                            response.Headers.Add("Authorization", "Bearer " + Request.Headers.Authorization?.Parameter);

                        return response;
                    }

                    requestRTM.v6 = jGroups.ToString(Formatting.None);
                    


                dataAccess = new DataAccess.InstanceService();
                respuesta = dataAccess.ExecStoreProcedure(requestRTM);

                if (respuesta == null)
                {
                    Service.StartVerifyCertificate();

                    response = Request.CreateResponse(HttpStatusCode.OK, new { IsSuccess = 0, Access = 1 });

                    if (Request.Headers.Authorization != null)
                        response.Headers.Add("Authorization", "Bearer " + Request.Headers.Authorization?.Parameter);

                    return response;
                }

                foreach (DataRow Row in respuesta.Tables[0].Rows)
                {
                    jsonString = jsonString + Row[0].ToString();
                }

                if (string.IsNullOrEmpty(jsonString))
                {
                    Service.StartVerifyCertificate();

                    response = Request.CreateResponse(HttpStatusCode.OK, new { IsSuccess = 0, Access = 1 });
                    if (Request.Headers.Authorization != null)
                        response.Headers.Add("Authorization", "Bearer " + Request.Headers.Authorization?.Parameter);

                    return response;
                }

                jsonResponse = JObject.Parse(jsonString);

                var token = jsonResponse.Value<string>("token");
                jsonResponse.Remove("token");

                response = Request.CreateResponse(HttpStatusCode.OK, jsonResponse);

                response.Headers.Add("Authorization", "Bearer " + token);

                return response;
            }
            catch (Exception e)
            {
                //Ups... Ha ocurrido un error
                Console.WriteLine(e.Message.ToString());
                response = Request.CreateResponse(HttpStatusCode.OK, new { IsSuccess = 0, Access = 1 });

                if (Request.Headers.Authorization != null)
                    response.Headers.Add("Authorization", "Bearer " + Request.Headers.Authorization?.Parameter);

                return response;
            }
        }

        [HttpPost]
        [Route("api/IG_smartMaps/IG_SmartMapsLoginPortalAdmin")]
        public HttpResponseMessage IG_SmartMapsLoginPortalAdmin([FromBody]Models.RequestPortalLogin request)
        {
            DataAccess.InstanceService dataAccess;
            HttpResponseMessage response;
            JObject jsonResponse;
            DataSet respuesta;
            object responseImage;
            Models.Response<int> responseUpload;
            string jsonString = string.Empty;
            Models.Request requestRTM = null;
            string esriTOken = string.Empty;
            try
            {
                requestRTM = new Models.Request();


                if (string.IsNullOrEmpty(Service.IsVerified))
                {
                    Service.StartVerifyCertificate();

                    //Petición no valida
                    response = Request.CreateResponse(HttpStatusCode.OK, new { IsSuccess = 0, Result = "Problemas con el certificado", Access = 1 });

                    if (Request.Headers.Authorization != null)
                        response.Headers.Add("Authorization", "Bearer " + Request.Headers.Authorization?.Parameter);

                    return response;
                }

                if (!ModelState.IsValid)
                {
                    //Petición no valida
                    response = Request.CreateResponse(HttpStatusCode.OK, new { IsSuccess = 0, Access = 1 });

                    if (Request.Headers.Authorization != null)
                        response.Headers.Add("Authorization", "Bearer " + Request.Headers.Authorization?.Parameter);

                    return response;
                }


                if (Request.Headers.Authorization == null || string.IsNullOrEmpty(Request.Headers.Authorization?.Parameter))
                {
                    //Petición no valida
                    response = Request.CreateResponse(HttpStatusCode.OK, new { IsSuccess = 0, Access = 1 });

                    if (Request.Headers.Authorization != null)
                        response.Headers.Add("Authorization", "Bearer " + Request.Headers.Authorization?.Parameter);

                    return response;
                }

                esriTOken = Request.Headers.Authorization?.Parameter;
                requestRTM.v2 = esriTOken;
                string urlPortal = ConfigurationManager.AppSettings["urlPortal"];
                Services.RestClient rest = new RestClient();
                rest.endPoint = $"{ urlPortal }/sharing/rest/portals/self?f=json&token={esriTOken}";
                rest.contentType = "application/Json";
                rest.httpMethod = httpVerb.POST;
                string result = rest.makeRequest();

                JObject valueResponse = JObject.Parse(result);

                JToken jGroups = null;

                JToken jUser = valueResponse.SelectToken("user");

                if (jUser.HasValues == false || jUser["username"] == null || string.IsNullOrEmpty(jUser.Value<string>("username")) || string.IsNullOrEmpty(jUser.Value<string>("fullName")))
                {
                    //Petición no valida
                    response = Request.CreateResponse(HttpStatusCode.OK, new { IsSuccess = 0, Access = 1 });

                    if (Request.Headers.Authorization != null)
                        response.Headers.Add("Authorization", "Bearer " + Request.Headers.Authorization?.Parameter);

                    return response;
                }


                requestRTM.v1 = "GetAuthenticationAdmin";
                requestRTM.v3 = jUser.Value<string>("username");
                requestRTM.v4 = jUser.Value<string>("fullName");
                requestRTM.v5 = jUser.Value<string>("email");

                rest = new RestClient();
                rest.endPoint = $"{ urlPortal }/sharing/rest/community/users/{jUser.Value<string>("username")}/?f=json&token={ esriTOken }";
                rest.contentType = "application/Json";
                rest.httpMethod = httpVerb.POST;
                result = rest.makeRequest();

                valueResponse = JObject.Parse(result);

                jGroups = valueResponse.SelectToken("groups");

                if (jGroups.HasValues == false)
                {
                    //Petición no valida
                    response = Request.CreateResponse(HttpStatusCode.OK, new { IsSuccess = 0, Access = 1 });

                    if (Request.Headers.Authorization != null)
                        response.Headers.Add("Authorization", "Bearer " + Request.Headers.Authorization?.Parameter);

                    return response;
                }

                requestRTM.v6 = jGroups.ToString(Formatting.None);



                dataAccess = new DataAccess.InstanceService();
                respuesta = dataAccess.ExecStoreProcedure(requestRTM);

                if (respuesta == null)
                {
                    Service.StartVerifyCertificate();

                    response = Request.CreateResponse(HttpStatusCode.OK, new { IsSuccess = 0, Access = 1 });

                    if (Request.Headers.Authorization != null)
                        response.Headers.Add("Authorization", "Bearer " + Request.Headers.Authorization?.Parameter);

                    return response;
                }

                foreach (DataRow Row in respuesta.Tables[0].Rows)
                {
                    jsonString = jsonString + Row[0].ToString();
                }

                if (string.IsNullOrEmpty(jsonString))
                {
                    Service.StartVerifyCertificate();

                    response = Request.CreateResponse(HttpStatusCode.OK, new { IsSuccess = 0, Access = 1 });
                    if (Request.Headers.Authorization != null)
                        response.Headers.Add("Authorization", "Bearer " + Request.Headers.Authorization?.Parameter);

                    return response;
                }

                jsonResponse = JObject.Parse(jsonString);

                var token = jsonResponse.Value<string>("token");
                jsonResponse.Remove("token");

                response = Request.CreateResponse(HttpStatusCode.OK, jsonResponse);

                response.Headers.Add("Authorization", "Bearer " + token);

                return response;
            }
            catch (Exception e)
            {
                //Ups... Ha ocurrido un error
                Console.WriteLine(e.Message.ToString());
                response = Request.CreateResponse(HttpStatusCode.OK, new { IsSuccess = 0, Access = 1 });

                if (Request.Headers.Authorization != null)
                    response.Headers.Add("Authorization", "Bearer " + Request.Headers.Authorization?.Parameter);

                return response;
            }
        }

        [HttpPost]
        public HttpResponseMessage IG_SmartMaps([FromBody]Models.Request request)
        {
            DataAccess.InstanceService dataAccess;
            HttpResponseMessage response;
            JObject jsonResponse;
            DataSet respuesta;
            object responseImage;
            Models.Response<int> responseUpload;
            string jsonString = string.Empty;
            try
            {
                if (string.IsNullOrEmpty(Service.IsVerified))
                {
                    Service.StartVerifyCertificate();

                    //Petición no valida
                    response = Request.CreateResponse(HttpStatusCode.OK, new { IsSuccess = 0, Result = "Problemas con el certificado", Access = 1 });

                    if (Request.Headers.Authorization != null)
                        response.Headers.Add("Authorization", "Bearer " + Request.Headers.Authorization?.Parameter);

                    return response;
                }

                request.v0 = Request.Headers.Authorization?.Parameter;
                if (!ModelState.IsValid)
                {
                    //Petición no valida
                    response = Request.CreateResponse(HttpStatusCode.OK, new { IsSuccess = 0, Access = 1 });

                    if (Request.Headers.Authorization != null)
                        response.Headers.Add("Authorization", "Bearer " + Request.Headers.Authorization?.Parameter);

                    return response;
                }

                if (request.v1.Equals("AddUserAppsDetails"))
                {
                    responseUpload = Helpers.FileHelper.UploadApk(request);

                    responseImage = new
                    {
                        IsSuccess = responseUpload.IsSuccess,
                        Access = responseUpload.Access,
                        Result = responseUpload.Result
                    };

                    response = Request.CreateResponse(HttpStatusCode.OK, responseImage);

                    response.Headers.Add("Authorization", "Bearer " + responseUpload.Token);

                    return response;
                }

                if (request.v1.Equals("UpdUserAppsDetails"))
                {
                    responseUpload = Helpers.FileHelper.ChangeMasterFile(request);

                    responseImage = new
                    {
                        IsSuccess = responseUpload.IsSuccess,
                        Access = responseUpload.Access,
                        Result = responseUpload.Result
                    };

                    response = Request.CreateResponse(HttpStatusCode.OK, responseImage);

                    response.Headers.Add("Authorization", "Bearer " + responseUpload.Token);

                    return response;
                }

                if (request.v1.Equals("UploadPhoto"))
                {
                    var res = Helpers.FileHelper.UploadImg(request);

                    responseImage = new
                    {
                        IsSuccess = res.IsSuccess,
                        Access = res.Access,
                        Result = res.Result
                    };

                    response = Request.CreateResponse(HttpStatusCode.OK, responseImage);

                    response.Headers.Add("Authorization", "Bearer " + res.Token);

                    return response;
                }

                dataAccess = new DataAccess.InstanceService();
                respuesta = dataAccess.ExecStoreProcedure(request);

                if (respuesta == null)
                {
                    Service.StartVerifyCertificate();

                    response = Request.CreateResponse(HttpStatusCode.OK, new { IsSuccess = 0, Access = 1 });

                    if (Request.Headers.Authorization != null)
                        response.Headers.Add("Authorization", "Bearer " + Request.Headers.Authorization?.Parameter);

                    return response;
                }

                foreach (DataRow Row in respuesta.Tables[0].Rows)
                {
                    jsonString = jsonString + Row[0].ToString();
                }

                if (string.IsNullOrEmpty(jsonString))
                {
                    Service.StartVerifyCertificate();

                    response = Request.CreateResponse(HttpStatusCode.OK, new { IsSuccess = 0, Access = 1 });
                    if (Request.Headers.Authorization != null)
                        response.Headers.Add("Authorization", "Bearer " + Request.Headers.Authorization?.Parameter);

                    return response;
                }

                jsonResponse = JObject.Parse(jsonString);

                var token = jsonResponse.Value<string>("token");
                jsonResponse.Remove("token");

                response = Request.CreateResponse(HttpStatusCode.OK, jsonResponse);

                response.Headers.Add("Authorization", "Bearer " + token);

                return response;
            }
            catch (Exception e)
            {
                //Ups... Ha ocurrido un error
                Console.WriteLine(e.Message.ToString());
                response = Request.CreateResponse(HttpStatusCode.OK, new { IsSuccess = 0, Access = 1 });

                if (Request.Headers.Authorization != null)
                    response.Headers.Add("Authorization", "Bearer " + Request.Headers.Authorization?.Parameter);

                return response;
            }
        }

        [HttpPost]
        [Route("api/IG_smartMaps/IG_SmartMapsJson")]
        public HttpResponseMessage IG_SmartMapsJson([FromBody]object request)
        {
            DataAccess.InstanceService dataAccess;
            HttpResponseMessage response;
            JObject jsonResponse;
            DataSet respuesta;
            object responseImage;
            Models.Response<int> responseUpload;
            string jsonString = string.Empty;
            try
            {
                if (string.IsNullOrEmpty(Service.IsVerified))
                {
                    Service.StartVerifyCertificate();

                    //Petición no valida
                    response = Request.CreateResponse(HttpStatusCode.Unauthorized, new { IsSuccess = 0, Result = "Failed", Message = "Licencia no válida.", Access = 1 });

                    if (Request.Headers.Authorization != null)
                        response.Headers.Add("Authorization", "Bearer " + Request.Headers.Authorization?.Parameter);

                    return response;
                }

                var request2 = new Models.Request
                {
                    v0 = Request.Headers.Authorization?.Parameter,
                    v1 = Request.Headers.GetValues("Method").FirstOrDefault(),
                    v2 = JsonConvert.SerializeObject(request),
                };

                if (!ModelState.IsValid)
                {
                    //Petición no valida
                    response = Request.CreateResponse(HttpStatusCode.BadRequest, new { Result = "Failed", IsSuccess = 0, Access = 1, jsonVar = JsonConvert.SerializeObject(request) });

                    if (Request.Headers.Authorization != null)
                        response.Headers.Add("Authorization", "Bearer " + Request.Headers.Authorization?.Parameter);

                    return response;
                }

                if (request2.v1 == ("AddUserAppsDetails"))
                {
                    responseUpload = Helpers.FileHelper.UploadApk(request2);

                    responseImage = new
                    {
                        IsSuccess = responseUpload.IsSuccess,
                        Access = responseUpload.Access,
                        Result = responseUpload.Result
                    };

                    response = Request.CreateResponse(HttpStatusCode.OK, responseImage);

                    response.Headers.Add("Authorization", "Bearer " + responseUpload.Token);

                    return response;
                }

                if (request2.v1 == ("UpdUserAppsDetails"))
                {
                    responseUpload = Helpers.FileHelper.ChangeMasterFile(request2);

                    responseImage = new
                    {
                        IsSuccess = responseUpload.IsSuccess,
                        Access = responseUpload.Access,
                        Result = responseUpload.Result
                    };

                    response = Request.CreateResponse(HttpStatusCode.OK, responseImage);

                    response.Headers.Add("Authorization", "Bearer " + responseUpload.Token);

                    return response;
                }

                if (request2.v1 == ("UploadPhoto"))
                {
                    var res = Helpers.FileHelper.UploadImg(request2);

                    responseImage = new
                    {
                        IsSuccess = res.IsSuccess,
                        Access = res.Access,
                        Result = res.Result
                    };

                    response = Request.CreateResponse(HttpStatusCode.OK, responseImage);

                    response.Headers.Add("Authorization", "Bearer " + res.Token);

                    return response;
                }



                dataAccess = new DataAccess.InstanceService();
                respuesta = dataAccess.ExecStoreProcedure(request2);

                if (respuesta == null)
                {
                    Service.StartVerifyCertificate();

                    response = Request.CreateResponse(HttpStatusCode.BadRequest, new {Result = "Failed", IsSuccess = 0, Access = 1, jsonVar = JsonConvert.SerializeObject(request) });
                    if (Request.Headers.Authorization != null)
                        response.Headers.Add("Authorization", "Bearer " + Request.Headers.Authorization?.Parameter);

                    return response;
                }

                foreach (DataRow Row in respuesta.Tables[0].Rows)
                {
                    jsonString = jsonString + Row[0].ToString();
                }

                if (string.IsNullOrEmpty(jsonString))
                {
                    Service.StartVerifyCertificate();

                    response = Request.CreateResponse(HttpStatusCode.BadRequest, new {Result= "Failed", IsSuccess = 0, Access = 1, jsonVar = JsonConvert.SerializeObject(request) });
                    if (Request.Headers.Authorization != null)
                        response.Headers.Add("Authorization", "Bearer " + Request.Headers.Authorization?.Parameter);

                    return response;
                }

                jsonResponse = JObject.Parse(jsonString);

                var token = jsonResponse.Value<string>("token");
                var resultJson = jsonResponse.Value<string>("Result");
                jsonResponse.Remove("token");
                

                if (string.IsNullOrEmpty(token))
                {
                    //Token no valido
                    //jsonResponse.Add(new { Result = "Token no valido..." });
                    response = Request.CreateResponse(HttpStatusCode.Unauthorized, new {jsonResponse});

                    if (Request.Headers.Authorization != null)
                        response.Headers.Add("Authorization", "Bearer " + Request.Headers.Authorization?.Parameter);

                    return response;
                }

                if (resultJson.Equals("Failed"))
                {
                    //Error de servidor
                    response = Request.CreateResponse(HttpStatusCode.BadRequest, new { jsonResponse });

                    if (Request.Headers.Authorization != null)
                        response.Headers.Add("Authorization", "Bearer " + Request.Headers.Authorization?.Parameter);

                    return response;
                }

                if (resultJson.Equals("Error"))
                {
                    //Error de servidor
                    response = Request.CreateResponse(HttpStatusCode.InternalServerError, new { jsonResponse });

                    if (Request.Headers.Authorization != null)
                        response.Headers.Add("Authorization", "Bearer " + Request.Headers.Authorization?.Parameter);

                    return response;
                }

                response = Request.CreateResponse(HttpStatusCode.OK, jsonResponse);

                response.Headers.Add("Authorization", "Bearer " + token);

                return response;
            }
            catch (Exception e)
            {
                //Ups... Ha ocurrido un error
                Console.WriteLine(e.Message.ToString());
                response = Request.CreateResponse(HttpStatusCode.InternalServerError, new {Result = "Error", IsSuccess = 0, Access = 1 , jsonVar = JsonConvert.SerializeObject(request) });

                if (Request.Headers.Authorization != null)
                    response.Headers.Add("Authorization", "Bearer " + Request.Headers.Authorization?.Parameter);

                return response;
            }
        }

        /*
         * Description:  Subir de imagen por form-data
         * 
         */
        [HttpPost]
        [Route("api/IG_smartMaps/IG_SmartMapsImge")]
        public HttpResponseMessage IG_SmartMapsImge()
        {
            DataAccess.InstanceService dataAccess;
            HttpResponseMessage response;
            string jsonString = string.Empty;
            string imageName;
            string photoDirection;
            string[] MIME = new string[]{
                "PNG",
                "JPG",
                "JPEG",
                "GIF",
                "TIFF",
                "BMP"
            };
            try
            {

                if (string.IsNullOrEmpty(Service.IsVerified))
                {
                    Service.StartVerifyCertificate();

                    //Petición no valida
                    response = Request.CreateResponse(HttpStatusCode.OK, new { IsSuccess = 0, Result = "Problemas con el certificado", Access = 1 });

                    if (Request.Headers.Authorization != null)
                        response.Headers.Add("Authorization", "Bearer " + Request.Headers.Authorization?.Parameter);

                    return response;
                }

                if (!HttpContext.Current.Request.Files.AllKeys.Any())
                {
                    response = Request.CreateResponse(HttpStatusCode.OK, new { IsSuccess = 0, Access = 1 });

                    if (Request.Headers.Authorization != null)
                        response.Headers.Add("Authorization", "Bearer " + Request.Headers.Authorization?.Parameter);

                    return response;
                }

                var httpPostedFile = HttpContext.Current.Request.Files["v3"];
                imageName = HttpContext.Current.Request.Form["v2"].Replace('-', '_');

                var ext = imageName.Split('.').Last();
                if (!MIME.Contains(ext.ToUpper()))
                {
                    response = Request.CreateResponse(HttpStatusCode.OK, new { IsSuccess = 0, Access = 1 });

                    if (Request.Headers.Authorization != null)
                        response.Headers.Add("Authorization", "Bearer " + Request.Headers.Authorization?.Parameter);

                    return response;
                }

                // Validate the uploaded APK
                if (httpPostedFile == null)
                {
                    response = Request.CreateResponse(HttpStatusCode.OK, new { IsSuccess = 0, Access = 1 });

                    if (Request.Headers.Authorization != null)
                        response.Headers.Add("Authorization", "Bearer " + Request.Headers.Authorization?.Parameter);

                    return response;
                }

                var request = new Models.Request()
                {
                    v0 = Request.Headers.Authorization?.Parameter,
                    v1 = HttpContext.Current.Request.Form["v1"],
                };

                dataAccess = new DataAccess.InstanceService();

                DataSet respuesta = dataAccess.ExecStoreProcedure(request);
                foreach (DataRow Row in respuesta.Tables[0].Rows)
                    jsonString = jsonString + Row[0].ToString();

                if (string.IsNullOrEmpty(jsonString))
                {
                    response = Request.CreateResponse(HttpStatusCode.OK, new { IsSuccess = 0, Access = 1 });

                    if (Request.Headers.Authorization != null)
                        response.Headers.Add("Authorization", "Bearer " + Request.Headers.Authorization?.Parameter);

                    return response;
                }

                Models.ImgUploadResponse imgUploadResponse = JsonConvert.DeserializeObject<Models.ImgUploadResponse>(jsonString);

                if (imgUploadResponse.IsSuccess.Equals(0))
                {
                    response = Request.CreateResponse(HttpStatusCode.OK, new { IsSuccess = 0, Access = 1 });

                    if (Request.Headers.Authorization != null)
                        response.Headers.Add("Authorization", "Bearer " + Request.Headers.Authorization?.Parameter);

                    return response;
                }

                string path = imgUploadResponse.Direction;
                ////set the image path

                photoDirection = Path.Combine(path, imageName);
                httpPostedFile.SaveAs(photoDirection);

                string photoDirectionThumbnail = Path.Combine(path, "thumbnail_" + imageName);
                Image image = Image.FromFile(photoDirection);
                Size thumbnailSize = GetThumbnailSize(image);
                Image thumbnail = image.GetThumbnailImage(thumbnailSize.Width, thumbnailSize.Height, null, IntPtr.Zero);
                thumbnail.Save(photoDirectionThumbnail);

                var responseSQL = new Models.Response<int>
                {
                    IsSuccess = 1,
                    Access = 1,
                    Result = imgUploadResponse.Result
                };

                response = Request.CreateResponse(HttpStatusCode.OK, responseSQL);

                response.Headers.Add("Authorization", "Bearer " + imgUploadResponse.token);

                return response;

            }
            catch (Exception ex)
            {
                response = Request.CreateResponse(HttpStatusCode.OK, new { IsSuccess = 0, Access = 1 });

                if (Request.Headers.Authorization != null)
                    response.Headers.Add("Authorization", "Bearer " + Request.Headers.Authorization?.Parameter);

                return response;
            }
        }
        /*
         * 
         * 
         */
        [HttpPatch]
        [Route("api/IG_smartMaps/IG_SmartMapsXLS")]
        public HttpResponseMessage IG_SmartMapsXLS()
        {
            DataAccess.InstanceService dataAccess;
            string jsonString = string.Empty;
            //string xlsName;
            string xlsDirection;
            HttpResponseMessage response;
            object response1;
            try
            {
                if (string.IsNullOrEmpty(Service.IsVerified))
                {
                    Service.StartVerifyCertificate();

                    //Petición no valida
                    response = Request.CreateResponse(HttpStatusCode.OK, new { IsSuccess = 0, Result = "Problemas con el certificado", Access = 1 });

                    if (Request.Headers.Authorization != null)
                        response.Headers.Add("Authorization", "Bearer " + Request.Headers.Authorization?.Parameter);

                    return response;
                }

                if (!HttpContext.Current.Request.Files.AllKeys.Any())
                {
                    response1 = new
                    {
                        IsSuccess = 0,
                        Access = 1,
                    };

                    response = Request.CreateResponse(HttpStatusCode.OK, response1);

                    if (Request.Headers.Authorization != null)
                        response.Headers.Add("Authorization", "Bearer " + Request.Headers.Authorization?.Parameter);

                    return response;
                }

                var httpPostedFile = HttpContext.Current.Request.Files["v1"];
                //xlsName = HttpContext.Current.Request.Form["v2"].Replace('-', '_');

                // Validate the uploaded APK
                if (httpPostedFile == null)
                {
                    response1 = new
                    {
                        IsSuccess = 0,
                        Access = 1,
                    };

                    response = Request.CreateResponse(HttpStatusCode.OK, response1);

                    if (Request.Headers.Authorization != null)
                        response.Headers.Add("Authorization", "Bearer " + Request.Headers.Authorization?.Parameter);

                    return response;
                }

                var request = new Models.Request()
                {
                    v0 = Request.Headers.Authorization?.Parameter,
                    v1 = "UploadXLS",
                    v2 = HttpContext.Current.Request.Form["v2"],
                    v3 = HttpContext.Current.Request.Form["v3"],
                };

                dataAccess = new DataAccess.InstanceService();

                DataSet respuesta = dataAccess.ExecStoreProcedure(request);
                foreach (DataRow Row in respuesta.Tables[0].Rows)
                    jsonString = jsonString + Row[0].ToString();

                if (string.IsNullOrEmpty(jsonString))
                {
                    response1 = new
                    {
                        IsSuccess = 0,
                        Access = 1,
                    };

                    response = Request.CreateResponse(HttpStatusCode.OK, response1);

                    if (Request.Headers.Authorization != null)
                        response.Headers.Add("Authorization", "Bearer " + Request.Headers.Authorization?.Parameter);

                    return response;
                }

                Models.ImgUploadResponse imgUploadResponse = JsonConvert.DeserializeObject<Models.ImgUploadResponse>(jsonString);

                if (imgUploadResponse.IsSuccess.Equals(0))
                {
                    response1 = new
                    {
                        IsSuccess = 0,
                        Access = 1,
                    };

                    response = Request.CreateResponse(HttpStatusCode.OK, response1);

                    if (Request.Headers.Authorization != null)
                        response.Headers.Add("Authorization", "Bearer " + Request.Headers.Authorization?.Parameter);

                    return response;
                }

                string path = imgUploadResponse.Direction;
                ////set the image path

                xlsDirection = path;//Path.Combine(path, xlsName);
                httpPostedFile.SaveAs(xlsDirection);


                FileStream stream = File.Open(xlsDirection, FileMode.Open, FileAccess.Read);
                IExcelDataReader excelReader;

                if (xlsDirection.ToUpper().Contains(".xlsx".ToUpper()))
                {
                    //2. Reading from a OpenXml Excel file (2007 format; *.xlsx)
                    excelReader = ExcelReaderFactory.CreateOpenXmlReader(stream);
                } //...
                else //if (fileSavePath.Contains(".xlsx"))
                {
                    //1. Reading from a binary Excel file ('97-2003 format; *.xls)
                    excelReader = ExcelReaderFactory.CreateBinaryReader(stream);
                } //...
                  //3. DataSet - The result of each spreadsheet will be created in the result.Tables
                  //DataSet result = excelReader.AsDataSet();
                  //...
                  //4. DataSet - Create column names from first row
                excelReader.IsFirstRowAsColumnNames = true;
                DataSet dataItineraryExcel = excelReader.AsDataSet();

                string json = JsonConvert.SerializeObject(dataItineraryExcel.Tables[0], Formatting.Indented);

                request = new Models.Request()
                {
                    v0 = imgUploadResponse.token,
                    v1 = imgUploadResponse.Function,
                    v2 = json
                };

                dataAccess = new DataAccess.InstanceService();

                respuesta = dataAccess.ExecStoreProcedure(request);
                foreach (DataRow Row in respuesta.Tables[0].Rows)
                    jsonString = jsonString + Row[0].ToString();

                if (string.IsNullOrEmpty(jsonString))
                {
                    response1 = new
                    {
                        IsSuccess = 0,
                        Access = 1,
                    };

                    response = Request.CreateResponse(HttpStatusCode.OK, response1);

                    if (Request.Headers.Authorization != null)
                        response.Headers.Add("Authorization", "Bearer " + Request.Headers.Authorization?.Parameter);

                    return response;
                }

                response1 = new
                {
                    IsSuccess = 1,
                    Access = 1,
                };

                response = Request.CreateResponse(HttpStatusCode.OK, response1);

                response.Headers.Add("Authorization", "Bearer " + imgUploadResponse.token);

                return response;
            }
            catch (Exception)
            {
                response1 = new
                {
                    IsSuccess = 0,
                    Access = 1,
                };

                response = Request.CreateResponse(HttpStatusCode.OK, response1);

                if (Request.Headers.Authorization != null)
                    response.Headers.Add("Authorization", "Bearer " + Request.Headers.Authorization?.Parameter);

                return response;
            }
        }

        [HttpPost]
        [Route("api/IG_smartMaps/IG_SmartMapsSync")]
        public HttpResponseMessage IG_SmartMapsSync([FromBody]Models.Request request)
        {
            DataAccess.InstanceService dataAccess;
            string jsonString = string.Empty;
            HttpResponseMessage response;
            object response1;
            try
            {
                if (string.IsNullOrEmpty(Service.IsVerified))
                {
                    Service.StartVerifyCertificate();

                    //Petición no valida
                    response = Request.CreateResponse(HttpStatusCode.OK, new { IsSuccess = 0, Result = "Problemas con el certificado", Access = 1 });

                    if (Request.Headers.Authorization != null)
                        response.Headers.Add("Authorization", "Bearer " + Request.Headers.Authorization?.Parameter);

                    return response;
                }

                if (string.IsNullOrEmpty(ConfigurationManager.AppSettings["Command"]))
                {
                    response1 = new
                    {
                        IsSuccess = 0,
                        Access = 1,
                    };

                    response = Request.CreateResponse(HttpStatusCode.OK, response1);

                    if (Request.Headers.Authorization != null)
                        response.Headers.Add("Authorization", "Bearer " + Request.Headers.Authorization?.Parameter);

                    return response;
                }

                string command = ConfigurationManager.AppSettings["Command"];

                dataAccess = new DataAccess.InstanceService();

                request.v1 = "GetValidatorToken";

                DataSet respuesta = dataAccess.ExecStoreProcedure(request);
                foreach (DataRow Row in respuesta.Tables[0].Rows)
                    jsonString = jsonString + Row[0].ToString();

                if (string.IsNullOrEmpty(jsonString))
                {
                    response1 = new
                    {
                        IsSuccess = 0,
                        Access = 1,
                    };

                    response = Request.CreateResponse(HttpStatusCode.OK, response1);

                    if (Request.Headers.Authorization != null)
                        response.Headers.Add("Authorization", "Bearer " + Request.Headers.Authorization?.Parameter);

                    return response;
                }

                Models.ImgUploadResponse objResponse = JsonConvert.DeserializeObject<Models.ImgUploadResponse>(jsonString);

                if (objResponse.IsSuccess.Equals(0))
                {
                    response1 = new
                    {
                        IsSuccess = 0,
                        Access = 1,
                    };

                    response = Request.CreateResponse(HttpStatusCode.OK, response1);

                    if (Request.Headers.Authorization != null)
                        response.Headers.Add("Authorization", "Bearer " + Request.Headers.Authorization?.Parameter);

                    return response;
                }

                using (Process p = new Process())
                {
                    p.StartInfo.FileName = "cmd.exe";
                    p.StartInfo.Arguments = command;
                    p.StartInfo.RedirectStandardInput = true;
                    p.StartInfo.RedirectStandardOutput = true;
                    p.StartInfo.CreateNoWindow = true;
                    p.StartInfo.UseShellExecute = false;
                    p.Start();
                    p.StandardInput.WriteLine(command);
                    p.StandardInput.Flush();
                    p.StandardInput.Close();
                    p.WaitForExit();
                }

                response1 = new
                {
                    IsSuccess = 1,
                    Access = 1,
                };

                response = Request.CreateResponse(HttpStatusCode.OK, response1);

                response.Headers.Add("Authorization", "Bearer " + objResponse.token);

                return response;
            }
            catch (Exception ex)
            {
                dataAccess = new DataAccess.InstanceService();
                dataAccess.ExecStoreProcedure(new Models.Request
                {
                    v0 = request.v0,
                    v1 = "AddLogTransaction",
                    v4 = ex.Message,
                });
                response1 = new
                {
                    IsSuccess = 0,
                    Access = 1,
                };

                response = Request.CreateResponse(HttpStatusCode.OK, response1);

                if (Request.Headers.Authorization != null)
                    response.Headers.Add("Authorization", "Bearer " + Request.Headers.Authorization?.Parameter);

                return response;
            }

        }

        /*
         * Descripcion: Subir de APK por form-data
         * 
         */
        [HttpPatch]
        [Route("api/IG_smartMaps/IG_SmartMapsFile")]
        public HttpResponseMessage IG_SmartMapsFile()
        {
            DataAccess.InstanceService dataAccess;
            string jsonString = string.Empty;
            HttpResponseMessage response;
            Object response1;
            try
            {
                if (string.IsNullOrEmpty(Service.IsVerified))
                {
                    Service.StartVerifyCertificate();

                    //Petición no valida
                    response = Request.CreateResponse(HttpStatusCode.OK, new { IsSuccess = 0, Result = "Problemas con el certificado", Access = 1 });

                    if (Request.Headers.Authorization != null)
                        response.Headers.Add("Authorization", "Bearer " + Request.Headers.Authorization?.Parameter);

                    return response;
                }

                if (!HttpContext.Current.Request.Files.AllKeys.Any())
                {
                    response1 = new 
                    {
                        IsSuccess = 0,
                        Access = 1,
                    };

                    response = Request.CreateResponse(HttpStatusCode.OK, response1);

                    if (Request.Headers.Authorization != null)
                        response.Headers.Add("Authorization", "Bearer " + Request.Headers.Authorization?.Parameter);

                    return response;
                }
 

                var httpPostedFile = HttpContext.Current.Request.Files["v5"];

                // Validate the uploaded APK
                if (httpPostedFile == null || !httpPostedFile.FileName.ToUpper().Contains(".APK"))
                {
                    response1 = new
                    {
                        IsSuccess = 0,
                        Access = 1,
                    };

                    response = Request.CreateResponse(HttpStatusCode.OK, response1);

                    if (Request.Headers.Authorization != null)
                        response.Headers.Add("Authorization", "Bearer " + Request.Headers.Authorization?.Parameter);

                    return response;
                }

                var request = new Models.Request()
                {
                    v0 = Request.Headers.Authorization?.Parameter,
                    v1 = HttpContext.Current.Request.Form["v1"],
                    v2 = HttpContext.Current.Request.Form["v2"],
                    v3 = HttpContext.Current.Request.Form["v3"],
                    v4 = HttpContext.Current.Request.Form["v4"]
                };

                dataAccess = new DataAccess.InstanceService();

                DataSet respuesta = dataAccess.ExecStoreProcedure(request);
                foreach (DataRow Row in respuesta.Tables[0].Rows)
                    jsonString = jsonString + Row[0].ToString();

                if (string.IsNullOrEmpty(jsonString))
                {
                    response1 = new
                    {
                        IsSuccess = 0,
                        Access = 1,
                    };

                    response = Request.CreateResponse(HttpStatusCode.OK, response1);

                    if (Request.Headers.Authorization != null)
                        response.Headers.Add("Authorization", "Bearer " + Request.Headers.Authorization?.Parameter);

                    return response;
                }

                Models.Response<Models.UserAppsDetailsResponse> uploadApkResponse = JsonConvert.DeserializeObject<Models.Response<Models.UserAppsDetailsResponse>>(jsonString);

                if (uploadApkResponse.IsSuccess.Equals(0))
                {
                    response1 = new
                    {
                        IsSuccess = 0,
                        Access = 1,
                    };

                    response = Request.CreateResponse(HttpStatusCode.OK, response1);

                    if (Request.Headers.Authorization != null)
                        response.Headers.Add("Authorization", "Bearer " + Request.Headers.Authorization?.Parameter);

                    return response;
                }

                ////set the image path

                var item = uploadApkResponse.Items[0];

                // Get the complete file path
                var fileSavePath = Path.Combine(item.OldAddress, item.OldFile.Replace("-", " ")); // HttpContext.Current.Server.MapPath("~/Uploads/") + Path.GetFileName(httpPostedFile.FileName);

                // Save the uploaded file to "UploadedFiles" folder
                httpPostedFile.SaveAs(fileSavePath);

                if (!string.IsNullOrEmpty(item.Address) && !string.IsNullOrEmpty(item.File))
                {
                    fileSavePath = Path.Combine(item.Address, item.File.Replace("-", " "));
                    httpPostedFile.SaveAs(fileSavePath);
                }

                using (Process p = new Process())
                {
                    p.StartInfo.FileName = "cmd.exe";
                    p.StartInfo.Arguments = "cd " + uploadApkResponse.Items[0].Address + " && git pull && git add . && git commit -m \"APP" + DateTime.Now.ToString() + "\"  && git push origin master";
                    p.StartInfo.RedirectStandardInput = true;
                    p.StartInfo.RedirectStandardOutput = true;
                    p.StartInfo.CreateNoWindow = true;
                    p.StartInfo.UseShellExecute = false;
                    p.Start();
                    string command = @"cd " + uploadApkResponse.Items[0].OldAddress + " && git pull && git add . && git commit -m " + "\"APP" + uploadApkResponse.Items[0].OldFile + "\"  && git push origin master";
                    p.StandardInput.WriteLine(command);
                    p.StandardInput.Flush();
                    p.StandardInput.Close();
                    p.WaitForExit();
                }

                response1 = new
                {
                    IsSuccess = 1,
                    Access = 1,
                };

                response = Request.CreateResponse(HttpStatusCode.OK, response1);

                response.Headers.Add("Authorization", "Bearer " + uploadApkResponse.Token);

                return response;
            }
            catch (Exception)
            {
                response = Request.CreateResponse(HttpStatusCode.OK, new { IsSuccess = 0, Access = 1 });

                if (Request.Headers.Authorization != null)
                    response.Headers.Add("Authorization", "Bearer " + Request.Headers.Authorization?.Parameter);

                return response;
            }
        }

        static Size GetThumbnailSize(Image original)
        {
            // Maximum size of any dimension.
            const int maxPixels = 150;
            // Width and height.
            int originalWidth = original.Width;
            int originalHeight = original.Height;
            // Compute best factor to scale entire image based on larger dimension.
            double factor;
            if (originalWidth > originalHeight)
            {
                factor = (double)maxPixels / originalWidth;
            }
            else
            {
                factor = (double)maxPixels / originalHeight;
            }
            // Return thumbnail size.
            return new Size((int)(originalWidth * factor), (int)(originalHeight * factor));
        }

        // Direccion no encotrada
        [HttpGet, HttpPut, HttpDelete, HttpHead, HttpOptions, AcceptVerbs(new string[] { "COPY", "LINK", "UNLINK", "PURGE", "LOCK", "UNLOCK", "PROPFIND", "VIEW" })]
        public HttpResponseMessage IG_SmartMaps()
        {
            var response = Request.CreateResponse(HttpStatusCode.OK, new { IsSuccess = 0, Access = 1 });

            if (Request.Headers.Authorization != null)
                response.Headers.Add("Authorization", "Bearer " + Request.Headers.Authorization?.Parameter);
            //response.Headers.Add("Authorization", Request.Headers.Authorization?.Parameter);

            return response;
        }

        [HttpPost]
        [Route("api/IG_smartMaps/IG_SmartMapsFolderLogs")]
        public IList<string> GetFolderNames()
        {
            //FIND ALL FOLDERS IN FOLDER with in own project
            string location = ConfigurationManager.AppSettings["dirLogs"].ToString();

            //For fixed path location will be like as string location =@"E:\Target Folder\";

            System.IO.DirectoryInfo dir = new System.IO.DirectoryInfo(@location);
            var folderList = new List<string>();

            foreach (System.IO.FileInfo g in dir.GetFiles())
            {
                //LOAD FOLDERS 
                folderList.Add(g.Name);
            }

            return folderList;
        }


        [HttpPost]
        [Route("api/IG_smartMaps/IG_SmartMapsFileLog")]
        public HttpResponseMessage IG_SmartMapsFileLog([FromBody]Models.Request fileName)
        {

            string path = Path.Combine(ConfigurationManager.AppSettings["dirLogs"].ToString(),fileName.v2);

            HttpResponseMessage result = new HttpResponseMessage(HttpStatusCode.OK);
            var stream = new FileStream(path, FileMode.Open, FileAccess.Read);
            result.Content = new StreamContent(stream);
            result.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("attachment")
            {
                FileName = fileName.v2
            };
            result.Content.Headers.ContentType = new MediaTypeHeaderValue("application/txt");
            return result;
        }
    }
}
