﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Configuration;
using System.Web.Http;
using System.Web.Http.Cors;
using com.Intelligis.KeyVerification;
using com.Intelligis.Common;
using System.IO;
using System.Data;
using Newtonsoft.Json.Linq;

namespace com.intelligis.core
{
    public static class WebApiConfig
    {

        public static void Register(HttpConfiguration config)
        {
            // Configuración y servicios de API web
            // Rutas de API web
            config.MapHttpAttributeRoutes();

            config.Routes.MapHttpRoute(
                name: "DefaultApi",
                routeTemplate: "api/IG_smartMaps",
                defaults: new { controller = "IG_smartMaps"}
            );

            /*
            config.MapHttpAttributeRoutes();
            config.Routes.MapHttpRoute(
                name: "FileApi",
                routeTemplate: "api/IG_Files/{action}",
                defaults: new { controller = "IG_File" }
            );*/

            config.Routes.MapHttpRoute(
                name: "Error404",
                routeTemplate: "{*url}",
                defaults: new { controller = "Error", action = "Handle404" }
            );

            config.IncludeErrorDetailPolicy = IncludeErrorDetailPolicy.Never;
        }
    }
}
