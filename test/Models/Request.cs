﻿//(C) Copyright 2020, GreenLight Telematics S.A.Todos los derechos reservados

using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace com.intelligis.core.Models
{
    public class Request
    {
        public string m0
        {
            get;
            set;
        }

        public string p0
        {
            get;
            set;
        }
        public string p1
        {
            get;
            set;
        }

        [MinLength(32)]
        [MaxLength(32)]
        public string v0
        {
            get;
            set;
        }
        [Required]
        public string v1
        {
            get;
            set;
        }
        public string v2
        {
            get;
            set;
        }
        public string v3
        {
            get;
            set;
        }
        public string v4
        {
            get;
            set;
        }
        public string v5
        {
            get;
            set;
        }
        public string v6
        {
            get;
            set;
        }
        public string v7
        {
            get;
            set;
        }
        public string v8
        {
            get;
            set;
        }
        public string v9
        {
            get;
            set;
        }
        public string v10
        {
            get;
            set;
        }
        public string v11
        {
            get;
            set;
        }
        public string v12
        {
            get;
            set;
        }
        public string v13
        {
            get;
            set;
        }
        public string v14
        {
            get;
            set;
        }
        public string v15
        {
            get;
            set;
        }
        public string v16
        {
            get;
            set;
        }
        public string v17
        {
            get;
            set;
        }
        public string v18
        {
            get;
            set;
        }
        public string v19
        {
            get;
            set;
        }
        public string v20
        {
            get;
            set;
        }
        public string v21
        {
            get;
            set;
        }
        public string v22
        {
            get;
            set;
        }
        public string v23
        {
            get;
            set;
        }
        public string v24
        {
            get;
            set;
        }
        public string v25
        {
            get;
            set;
        }
        public string v26
        {
            get;
            set;
        }

        public string v27
        {
            get;
            set;
        }

        public string v28
        {
            get;
            set;
        }
        public string v29
        {
            get;
            set;
        }
        public string v30
        {
            get;
            set;
        }
        public string v31
        {
            get;
            set;
        }
        public string v32
        {
            get;
            set;
        }
        public string v33
        {
            get;
            set;
        }
        public string v34
        {
            get;
            set;
        }
        public string v35
        {
            get;
            set;
        }
        public string v36
        {
            get;
            set;
        }
        public string v37
        {
            get;
            set;
        }
        public string v38
        {
            get;
            set;
        }
        public string v39
        {
            get;
            set;
        }
        public string v40
        {
            get;
            set;
        }
        public string v41
        {
            get;
            set;
        }
        public string v42
        {
            get;
            set;
        }
        public string v43
        {
            get;
            set;
        }
        public string v44
        {
            get;
            set;
        }
        public string v45
        {
            get;
            set;
        }
        public string v46
        {
            get;
            set;
        }
        public string v47
        {
            get;
            set;
        }
        public string v48
        {
            get;
            set;
        }
        public string v49
        {
            get;
            set;
        }
        public string v50
        {
            get;
            set;
        }
        public string v51
        {
            get;
            set;
        }
        public string v52
        {
            get;
            set;
        }
        public string v53
        {
            get;
            set;
        }
        public string v54
        {
            get;
            set;
        }
        public string v55
        {
            get;
            set;
        }
        public string v56
        {
            get;
            set;
        }
        public string v57
        {
            get;
            set;
        }
        public string v58
        {
            get;
            set;
        }
        public string v59
        {
            get;
            set;
        }
        public string v60
        {
            get;
            set;
        }
    }
}