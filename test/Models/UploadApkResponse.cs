﻿//(C) Copyright 2020, GreenLight Telematics S.A.Todos los derechos reservados

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace com.intelligis.core.Models
{
    public class Response<T>
    {
        public string Token { get; set; }
        public int Access { get; set; }
        public string Result { get; set; }
        public int IsSuccess { get; set; }
        public List<T> Items { get; set; }
    }

    public class UserAppsDetailsResponse
    {
        public string Address { get; set; }
        public string OldAddress { get; set; }
        public string OldFile { get; set; }
        public string File { get; set; }
        public string Type { get; set; }
    }
}