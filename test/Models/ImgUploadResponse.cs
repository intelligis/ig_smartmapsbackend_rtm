﻿//(C) Copyright 2020, GreenLight Telematics S.A.Todos los derechos reservados

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace com.intelligis.core.Models
{
    public class ImgUploadResponse
    {
        public string token { get; set; }
        public int Access { get; set; }
        public string Result { get; set; }
        public int IsSuccess { get; set; }
        public string Direction { get; set; }
        public string Function { get; set; }
    }
}