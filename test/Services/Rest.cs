﻿//(C) Copyright 2020, GreenLight Telematics S.A.Todos los derechos reservados

using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;

namespace com.intelligis.core.Services
{
    class Rest
    {

        private HttpClient _httpClient = new HttpClient();

        public string POSTData(string urlHost, object object_, string token)
        {
            string returnValue = null;
            using (var content = new StringContent(JsonConvert.SerializeObject(object_), System.Text.Encoding.UTF8, "application/json"))
            {
                if (!String.IsNullOrEmpty(token))
                    _httpClient.DefaultRequestHeaders.Add("Authorization", token);
                HttpResponseMessage result = _httpClient.PostAsync(urlHost, content).Result;
                if (result.StatusCode == System.Net.HttpStatusCode.OK)
                {
                    returnValue = result.Content.ReadAsStringAsync().Result;
                }
                else
                {
                    throw new Exception($"Failed to POST data: ({result.StatusCode}): {returnValue}");
                }
            }
            return returnValue;
        }
        public string POSTData(string urlHost, string json, string token)
        {
            string returnValue = null;
            using (var content = new StringContent(json, System.Text.Encoding.UTF8, "application/json"))
            //using (var content = new StringContent(json))
            {
                if(!string.IsNullOrEmpty(token))
                    _httpClient.DefaultRequestHeaders.Add("Authorization", token);
                //_httpClient.DefaultRequestHeaders.Accept.Add(
                //    new MediaTypeWithQualityHeaderValue("application/json"));
                HttpResponseMessage result = _httpClient.PostAsync(urlHost, content).Result;
                if (result.StatusCode == System.Net.HttpStatusCode.OK)
                {
                    returnValue = result.Content.ReadAsStringAsync().Result;
                }
                else
                {
                    throw new Exception($"Failed to POST data: ({result.StatusCode}): {returnValue}");
                }
            }
            return returnValue;
        }
    }


}
