﻿//(C) Copyright 2020, GreenLight Telematics S.A.Todos los derechos reservados

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Timers;
using System.Configuration;
using System.IO;
using com.Intelligis.KeyVerification;
using com.Intelligis.Common;
using Newtonsoft.Json;
using System.Data;
using System.Threading.Tasks;

namespace com.intelligis.core.Services
{
    class Service
    {
        public static string IsVerified;
        private static Timer timer;

        public static void StartVerifyCertificate()
        {
            VerifyCertificate(null, null);
            timer = new Timer(TimeSpan.FromDays(1).TotalMilliseconds);
            timer.Elapsed += VerifyCertificate;
            timer.AutoReset = true;
            timer.Enabled = true;
            timer.Start();
        }

        static void VerifyCertificate(Object source, ElapsedEventArgs e)
        {
            try
            {
                //Valida que el archivo este en el Web.config
                if (!ConfigurationManager.AppSettings.AllKeys.Contains("File"))
                {
                    IsVerified = string.Empty;
                    GetLicense();
                    return;
                }

                //Obtiene y valida que el archivo exista
                var file = ConfigurationManager.AppSettings["File"];
                if (string.IsNullOrEmpty(file))
                {
                    IsVerified = string.Empty;
                    GetLicense();
                    return;
                }

                //Valida que el archivo exista
                if (!File.Exists(file))
                {
                    IsVerified = string.Empty;
                    GetLicense();
                    return;
                }

                //Lee el archivo y ve si tiene dos lineas
                string[] lines = File.ReadAllLines(file);
                if (lines.Length < 2)
                {
                    IsVerified = string.Empty;
                    GetLicense();
                    return;
                }

                //Obtiene el certificado
                var firstLine = lines[0];

                //Obtiene las demas lineas (estas lineas tienen el nombre del equipo y del usuario cifrados)
                lines[0] = string.Empty;
                string strings = string.Empty;
                foreach (string line in lines)
                    strings += line;

                //Valida el cerificado
                var pkvKeyCheck = new PkvKeyCheck();
                var keyBytes = new[] {
                    new KeyByteSet(5, 165, 15, 28),
                    new KeyByteSet(6, 99, 175, 213)
                };

                //Metodo para validar el certificado
                var result = pkvKeyCheck.CheckKey(firstLine, keyBytes, 8, null);
                if (result != PkvLicenceKeyResult.KeyGood)
                {
                    IsVerified = string.Empty;
                    GetLicense();
                    return;
                }

                //Valida el certificado en la base de datos
                var request = new com.intelligis.core.Models.Request()
                {
                    v1 = "GetValidate",
                    v2 = strings,
                    v3 = Environment.MachineName,
                    v4 = System.Reflection.Assembly.GetExecutingAssembly().GetName().Name,
                    v5 = System.Reflection.Assembly.GetExecutingAssembly().GetName().Version.ToString()
                };

                //Ejecuta el SP
                DataAccess.InstanceService dataAccess = new DataAccess.InstanceService();
                var respuesta = dataAccess.ExecStoreProcedure(request);
                string jsonString = string.Empty;

                //Obtiene la respuesta de la base de datos
                foreach (DataRow Row in respuesta.Tables[0].Rows)
                {
                    jsonString = jsonString + Row[0].ToString();
                }


                //Valida el token del certificado
                if (string.IsNullOrEmpty(jsonString))
                {
                    IsVerified = string.Empty;
                    GetLicense();
                    return;
                }

                //Guarda el token del certificado
                IsVerified = jsonString;

                //Duerme hasta el otro dia
                //await Task.Delay(TimeSpan.FromDays(1));

                //Se ejecuta de nuevo
                //VerifyCertificate();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        static void GetLicense()
        {
            if (!ConfigurationManager.AppSettings.AllKeys.Contains("PreActivate"))
            {
                IsVerified = string.Empty;
                return;
            }

            if (string.IsNullOrEmpty(ConfigurationManager.AppSettings["PreActivate"]))
            {
                IsVerified = string.Empty;
                return;
            }

            using (StreamWriter PreActivate = new StreamWriter(ConfigurationManager.AppSettings["PreActivate"]))
            {
                PreActivate.WriteLine(Environment.MachineName);
                PreActivate.WriteLine(System.Reflection.Assembly.GetExecutingAssembly().GetName().Name);
                PreActivate.WriteLine(System.Reflection.Assembly.GetExecutingAssembly().GetName().Version.ToString());
            }
        }
    }
}