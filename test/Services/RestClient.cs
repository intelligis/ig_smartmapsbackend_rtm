﻿//(C) Copyright 2020, GreenLight Telematics S.A.Todos los derechos reservados

using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace com.intelligis.core.Services
{
 
        class AccessToken
        {
            public String tokenValue { get; set; }
            public String value { get; set; }
        }
        public enum httpVerb
        {
            //Listado de posibles acciones
            GET,
            POST,
            PUT,
            DELETE
        }
        class RestClient
        {
            public string endPoint { get; set; } //direccion del servicio
            public httpVerb httpMethod { get; set; }
            public string parameters { get; set; }
            public string token { get; set; } //puede ir vacio

            public string cookie { get; set; } //puede ir vacio

            public string contentType { get; set; } //puede ir vacio

        public RestClient()
            {
                endPoint = string.Empty;
                httpMethod = httpVerb.GET;
                parameters = string.Empty;
                token = string.Empty;
                cookie = string.Empty;
            }
            public string makeRequest()
            {
                ServicePointManager.SecurityProtocol = SecurityProtocolType.Ssl3 | SecurityProtocolType.Tls12 | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls;

                string strResponseValue = string.Empty;
                if (httpMethod.ToString() == "GET" && parameters != string.Empty)
                {
                    endPoint = endPoint + "?" + parameters; //si es get envia los parametros como querystring
                }
                HttpWebRequest request = (HttpWebRequest)HttpWebRequest.Create(endPoint);
                request.Method = httpMethod.ToString();
                request.ContentLength = 0;
                var data = Encoding.ASCII.GetBytes(parameters); //codifica los parametros
                if (request.Method == "POST" || request.Method == "PUT")
                {
                if (string.IsNullOrEmpty(contentType))
                    request.ContentType = "application/json; charset=utf-8"; //especifica el tipo de contenido
                else
                    request.ContentType = contentType;

                    request.ContentLength = data.Length;
                    using (var stream = request.GetRequestStream())
                    {
                        stream.Write(data, 0, data.Length);
                    }
                }
                else
                    request.ContentType = "application/json";

                if (token != string.Empty)
                {
                    request.Headers.Add(HttpRequestHeader.Authorization, token); //si hay un token especificado agrega Authorization al header
                    //request.Headers.Add("Content-Type", "application/x-www-form-urlencoded");

                }
                if (cookie != string.Empty)
                {
                    request.Headers.Add("Cookie", cookie); //si hay un Cookie especificado agrega Authorization al header
                }
                using (HttpWebResponse response = (HttpWebResponse)request.GetResponse())
                {
                    if (response.StatusCode != HttpStatusCode.OK && response.StatusCode != HttpStatusCode.Created)
                    {
                        throw new ApplicationException("ERROR CORDE: " + response.StatusCode.ToString());
                    }
                    if(string.IsNullOrEmpty(token))
                        token = response.Headers[1];
                    using (Stream responseStream = response.GetResponseStream())
                    {
                        if (responseStream != null)
                        {
                            using (StreamReader reader = new StreamReader(responseStream))
                            {
                                strResponseValue = reader.ReadToEnd();
                            }
                        }
                    }
                }
                return strResponseValue;
            }
        }
    
}
