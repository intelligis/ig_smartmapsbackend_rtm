﻿//(C) Copyright 2020, GreenLight Telematics S.A.Todos los derechos reservados
using System;
using System.Web;
using System.Diagnostics;
using System.IO;
using System.Data;
using Newtonsoft.Json;
using System.Collections.Generic;

namespace com.intelligis.core.Helpers
{
    public class FileHelper
    {
        public static Models.Response<string> UploadImg(Models.Request request)
        {
            DataAccess.InstanceService dataAccess;
            string jsonString = string.Empty;
            string image;
            string imageName;
            String path = null;
            string photoDirection;
            DataSet respuesta;
            try
            {
                //path = HttpContext.Current.Server.MapPath("~/Uploads"); //Path
                if (!string.IsNullOrEmpty(request.v2) && !string.IsNullOrEmpty(request.v3))
                {
                    image = request.v3;
                    imageName = request.v2.Replace('-', '_');
                    request.v2 = null;
                    request.v3 = null;

                    dataAccess = new DataAccess.InstanceService();
                    respuesta = dataAccess.ExecStoreProcedure(request);

                    foreach (DataRow Row in respuesta.Tables[0].Rows)
                        jsonString = jsonString + Row[0].ToString();

                    if (string.IsNullOrEmpty(jsonString))
                        return new Models.Response<string>  { Token = "", IsSuccess = 0, Access = 1 };

                    Models.ImgUploadResponse imgUploadResponse = JsonConvert.DeserializeObject<Models.ImgUploadResponse>(jsonString);

                    if (string.IsNullOrEmpty(imgUploadResponse.token) || string.IsNullOrEmpty(imgUploadResponse.Direction))
                        return new Models.Response<string> { Token = "", IsSuccess = 0, Access = 1 };

                    path = imgUploadResponse.Direction;
                    ////set the image path
                    photoDirection = Path.Combine(path, imageName);

                    byte[] imageBytes = Convert.FromBase64String(image);

                    File.WriteAllBytes(photoDirection, imageBytes);

                    return new Models.Response<string> { Token = imgUploadResponse.token, IsSuccess = 1, Access = 1, Items = new List<string> { imageName } };
                }
                return new Models.Response<string> { Token = "", IsSuccess = 0, Access = 1 };
            }
            catch (Exception)
            {
                return new Models.Response<string> { Token = "", IsSuccess = 0, Access = 1 };
            }
        }
        /*
         * Es usado para subir las aplicaciones moviles.
         *      v2: es el nombre de la aplicación
         *      v3: es la aplicación en Base64
         */
        public static Models.Response<int> UploadApk(Models.Request request)
        {
            //string apkRelease_ = null;
            string apkHistory_ = null;
            string apkRelease_ = null;
            DataAccess.InstanceService dataAccess;
            byte[] imageBytes;
            string jsonString = string.Empty;
            try
            {
                if (string.IsNullOrEmpty(request.v5))
                    return new Models.Response<int>
                    {
                        Token = string.Empty,
                        IsSuccess = 1,
                        Access = 1,
                        Result = "Failed"
                    };

                string apkBase64 = request.v5;
                request.v5 = null;

                // Validate the uploaded image(optional)
                //if (!request.v2.Contains(".apk"))
                //    return new { token = "", IsSuccess = 0, Access = 1 };

                dataAccess = new DataAccess.InstanceService();

                DataSet respuesta = dataAccess.ExecStoreProcedure(request);
                foreach (DataRow Row in respuesta.Tables[0].Rows)
                    jsonString = jsonString + Row[0].ToString();

                if (string.IsNullOrEmpty(jsonString))
                    return new Models.Response<int>
                    {
                        Token = string.Empty,
                        IsSuccess = 1,
                        Access = 1,
                        Result = "Failed"
                    };

                Models.Response<Models.UserAppsDetailsResponse> uploadApkResponse = JsonConvert.DeserializeObject<Models.Response<Models.UserAppsDetailsResponse>>(jsonString);

                if (uploadApkResponse.IsSuccess.Equals(0))
                    return new Models.Response<int>
                    {
                        Token = uploadApkResponse.Token,
                        IsSuccess = 0,
                        Access = 1,
                        Result = uploadApkResponse.Result
                    };

                ////set the image path

                var item = uploadApkResponse.Items[0];

                if (item == null)
                    return new Models.Response<int>
                    {
                        Token = uploadApkResponse.Token,
                        IsSuccess = 0,
                        Access = 1,
                        Result = uploadApkResponse.Result
                    };

                apkHistory_ = Path.Combine(item.OldAddress, item.OldFile.Replace("-", " "));
                imageBytes = Convert.FromBase64String(apkBase64.Replace("data:image/png;base64,", ""));
                File.WriteAllBytes(apkHistory_, imageBytes);

                if (!string.IsNullOrEmpty(item.Address) && !string.IsNullOrEmpty(item.File))
                {
                    apkRelease_ = Path.Combine(item.Address, item.File);
                    imageBytes = Convert.FromBase64String(apkBase64.Replace("data:image/png;base64,", ""));
                    File.WriteAllBytes(apkRelease_, imageBytes);
                }

                using (Process p = new Process())
                {
                    p.StartInfo.FileName = "cmd.exe";
                    p.StartInfo.Arguments = "cd " + uploadApkResponse.Items[0].Address + " && git pull && git add . && git commit -m \"APP" + DateTime.Now.ToString() + "\"  && git push origin master";
                    p.StartInfo.RedirectStandardInput = true;
                    p.StartInfo.RedirectStandardOutput = true;
                    p.StartInfo.CreateNoWindow = true;
                    p.StartInfo.UseShellExecute = false;
                    p.Start();
                    string command = @"cd "+ uploadApkResponse.Items[0].OldAddress + " && git pull && git add . && git commit -m " + "\"APP" + uploadApkResponse.Items[0].OldFile + "\"  && git push origin master";
                    p.StandardInput.WriteLine(command);
                    p.StandardInput.Flush();
                    p.StandardInput.Close();
                    p.WaitForExit();
                }
                return new Models.Response<int>
                {
                    Token = uploadApkResponse.Token,
                    IsSuccess = 1,
                    Access = 1,
                    Result = uploadApkResponse.Result
                };
            }
            catch (Exception e)
            {
                //Ups... Ha ocurrido un error
                Console.WriteLine(e.Message.ToString());
                return new Models.Response<int>
                {
                    Token = string.Empty,
                    IsSuccess = 1,
                    Access = 1,
                    Result = "Failed"
                };
            }
        }

        /*
         * Cambia la aplicación movil principal
         *      v2: es el nombre de la aplicación que sera la principal
         */
        public static Models.Response<int> ChangeMasterFile(Models.Request request)
        {
            DataAccess.InstanceService dataAccess;
            string jsonString = string.Empty;

            // Delete a file by using File class static method...

            // Use a try block to catch IOExceptions, to
            // handle the case of the file already being
            // opened by another process.
            try
            {
                dataAccess = new DataAccess.InstanceService();
                dataAccess.ExecStoreProcedure(request);

                DataSet respuesta = dataAccess.ExecStoreProcedure(request);
                foreach (DataRow Row in respuesta.Tables[0].Rows)
                {
                    jsonString = jsonString + Row[0].ToString();
                }

                if (string.IsNullOrEmpty(jsonString))
                    return new Models.Response<int> { Token = string.Empty, IsSuccess = 1, Access = 1, Result = "Failed" };

                Models.Response<Models.UserAppsDetailsResponse> updUserAppsDetailsResponse = JsonConvert.DeserializeObject<Models.Response<Models.UserAppsDetailsResponse>>(jsonString);

                if (updUserAppsDetailsResponse.IsSuccess.Equals(0))
                    return new Models.Response<int> { Token = updUserAppsDetailsResponse.Token, IsSuccess = 1, Access = 1, Result = updUserAppsDetailsResponse.Result };

                var item = updUserAppsDetailsResponse.Items[0];

                if (item == null)
                    return new Models.Response<int>{ Token = updUserAppsDetailsResponse.Token, IsSuccess = 0, Access = 1, Result = "Failed" };

                string fileName = item.File;
                string oldFileName = item.OldFile;
                string address = item.Address; //HttpContext.Current.Server.MapPath("~/Uploads/");
                string oldAddress = item.OldAddress; //HttpContext.Current.Server.MapPath("~/Master/");

                // Use Path class to manipulate file and directory paths.
                address = Path.Combine(address, fileName); //Principal
                oldAddress = Path.Combine(oldAddress, oldFileName); // Historico

                if (File.Exists(address))
                    File.Delete(address);

                File.Copy(oldAddress, address, false);
                return new Models.Response<int> { Token = updUserAppsDetailsResponse.Token, IsSuccess = 1, Access = 1, Result = updUserAppsDetailsResponse.Result };
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                return new Models.Response<int> { Token = string.Empty, IsSuccess = 1, Access = 1, Result = "Failed" };
            }
        }
    }
}