﻿
//(C) Copyright 2020, GreenLight Telematics S.A.Todos los derechos reservados
using com.intelligis.core.Services;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Web;

namespace com.intelligis.core.DataAccess
{
    public class InstanceService
    {
        public SqlConnection fpucnnSQLConexion()
        {
            SqlConnection lcnnConexion;
            try
            {
                lcnnConexion = new SqlConnection();
                lcnnConexion.ConnectionString = ConfigurationManager.AppSettings["ConnectionSQL"];
                lcnnConexion.Open();
                return lcnnConexion;
            }
            catch (Exception e)
            {
                throw new Exception();
            }
        }
        public DataSet ExecStoreProcedure(Models.Request request)
        {
            SqlCommand sqlCommand;
            SqlDataAdapter dataAdapter;
            DataSet result;
            try
            {
                sqlCommand = new SqlCommand("dbo." + request.v1, fpucnnSQLConexion());
                sqlCommand.CommandType = CommandType.StoredProcedure;

                dataAdapter = new SqlDataAdapter(sqlCommand);
                result = new DataSet();


                if (!String.IsNullOrEmpty(Service.IsVerified))
                    sqlCommand.Parameters.AddWithValue("@l0", Service.IsVerified);
                if (!String.IsNullOrEmpty(request.m0))
                    sqlCommand.Parameters.AddWithValue("@m0", request.m0);
                if (!String.IsNullOrEmpty(request.p0))
                    sqlCommand.Parameters.AddWithValue("@p0", request.p0);
                if (!String.IsNullOrEmpty(request.p1))
                    sqlCommand.Parameters.AddWithValue("@p1", request.p1);
                if (!String.IsNullOrEmpty(request.v0))
                    sqlCommand.Parameters.AddWithValue("@v0", request.v0);
                if (!String.IsNullOrEmpty(request.v2))
                    sqlCommand.Parameters.AddWithValue("@v2", request.v2);
                if (!String.IsNullOrEmpty(request.v3))
                    sqlCommand.Parameters.AddWithValue("@v3", request.v3);
                if (!String.IsNullOrEmpty(request.v4))
                    sqlCommand.Parameters.AddWithValue("@v4", request.v4);
                if (!String.IsNullOrEmpty(request.v5))
                    sqlCommand.Parameters.AddWithValue("@v5", request.v5);
                if (!String.IsNullOrEmpty(request.v6))
                    sqlCommand.Parameters.AddWithValue("@v6", request.v6);
                if (!String.IsNullOrEmpty(request.v7))
                    sqlCommand.Parameters.AddWithValue("@v7", request.v7);
                if (!String.IsNullOrEmpty(request.v8))
                    sqlCommand.Parameters.AddWithValue("@v8", request.v8);
                if (!String.IsNullOrEmpty(request.v9))
                    sqlCommand.Parameters.AddWithValue("@v9", request.v9);
                if (!String.IsNullOrEmpty(request.v10))
                    sqlCommand.Parameters.AddWithValue("@v10", request.v10);
                if (!String.IsNullOrEmpty(request.v11))
                    sqlCommand.Parameters.AddWithValue("@v11", request.v11);
                if (!String.IsNullOrEmpty(request.v12))
                    sqlCommand.Parameters.AddWithValue("@v12", request.v12);
                if (!String.IsNullOrEmpty(request.v13))
                    sqlCommand.Parameters.AddWithValue("@v13", request.v13);
                if (!String.IsNullOrEmpty(request.v14))
                    sqlCommand.Parameters.AddWithValue("@v14", request.v14);
                if (!String.IsNullOrEmpty(request.v15))
                    sqlCommand.Parameters.AddWithValue("@v15", request.v15);
                if (!String.IsNullOrEmpty(request.v16))
                    sqlCommand.Parameters.AddWithValue("@v16", request.v16);
                if (!String.IsNullOrEmpty(request.v17))
                    sqlCommand.Parameters.AddWithValue("@v17", request.v17);
                if (!String.IsNullOrEmpty(request.v18))
                    sqlCommand.Parameters.AddWithValue("@v18", request.v18);
                if (!String.IsNullOrEmpty(request.v19))
                    sqlCommand.Parameters.AddWithValue("@v19", request.v19);
                if (!String.IsNullOrEmpty(request.v20))
                    sqlCommand.Parameters.AddWithValue("@v20", request.v20);
                if (!String.IsNullOrEmpty(request.v21))
                    sqlCommand.Parameters.AddWithValue("@v21", request.v21);
                if (!String.IsNullOrEmpty(request.v22))
                    sqlCommand.Parameters.AddWithValue("@v22", request.v22);
                if (!String.IsNullOrEmpty(request.v23))
                    sqlCommand.Parameters.AddWithValue("@v23", request.v23);
                if (!String.IsNullOrEmpty(request.v24))
                    sqlCommand.Parameters.AddWithValue("@v24", request.v24);
                if (!String.IsNullOrEmpty(request.v25))
                    sqlCommand.Parameters.AddWithValue("@v25", request.v25);
                if (!String.IsNullOrEmpty(request.v26))
                    sqlCommand.Parameters.AddWithValue("@v26", request.v26);
                if (!String.IsNullOrEmpty(request.v27))
                    sqlCommand.Parameters.AddWithValue("@v27", request.v27);
                if (!String.IsNullOrEmpty(request.v28))
                    sqlCommand.Parameters.AddWithValue("@v28", request.v28);
                if (!String.IsNullOrEmpty(request.v29))
                    sqlCommand.Parameters.AddWithValue("@v29", request.v29);
                if (!String.IsNullOrEmpty(request.v30))
                    sqlCommand.Parameters.AddWithValue("@v30", request.v30);
                if (!String.IsNullOrEmpty(request.v31))
                    sqlCommand.Parameters.AddWithValue("@v31", request.v31);
                if (!String.IsNullOrEmpty(request.v32))
                    sqlCommand.Parameters.AddWithValue("@v32", request.v32);
                if (!String.IsNullOrEmpty(request.v33))
                    sqlCommand.Parameters.AddWithValue("@v33", request.v33);
                if (!String.IsNullOrEmpty(request.v34))
                    sqlCommand.Parameters.AddWithValue("@v34", request.v34);
                if (!String.IsNullOrEmpty(request.v35))
                    sqlCommand.Parameters.AddWithValue("@v35", request.v35);
                if (!String.IsNullOrEmpty(request.v36))
                    sqlCommand.Parameters.AddWithValue("@v36", request.v36);
                if (!String.IsNullOrEmpty(request.v37))
                    sqlCommand.Parameters.AddWithValue("@v37", request.v37);
                if (!String.IsNullOrEmpty(request.v38))
                    sqlCommand.Parameters.AddWithValue("@v38", request.v38);
                if (!String.IsNullOrEmpty(request.v39))
                    sqlCommand.Parameters.AddWithValue("@v39", request.v39);
                if (!String.IsNullOrEmpty(request.v40))
                    sqlCommand.Parameters.AddWithValue("@v40", request.v40);
                if (!String.IsNullOrEmpty(request.v41))
                    sqlCommand.Parameters.AddWithValue("@v41", request.v41);
                if (!String.IsNullOrEmpty(request.v42))
                    sqlCommand.Parameters.AddWithValue("@v42", request.v42);
                if (!String.IsNullOrEmpty(request.v43))
                    sqlCommand.Parameters.AddWithValue("@v43", request.v43);
                if (!String.IsNullOrEmpty(request.v44))
                    sqlCommand.Parameters.AddWithValue("@v44", request.v44);
                if (!String.IsNullOrEmpty(request.v45))
                    sqlCommand.Parameters.AddWithValue("@v45", request.v45);
                if (!String.IsNullOrEmpty(request.v46))
                    sqlCommand.Parameters.AddWithValue("@v46", request.v46);
                if (!String.IsNullOrEmpty(request.v47))
                    sqlCommand.Parameters.AddWithValue("@v47", request.v47);
                if (!String.IsNullOrEmpty(request.v48))
                    sqlCommand.Parameters.AddWithValue("@v48", request.v48);
                if (!String.IsNullOrEmpty(request.v49))
                    sqlCommand.Parameters.AddWithValue("@v49", request.v49);
                if (!String.IsNullOrEmpty(request.v50))
                    sqlCommand.Parameters.AddWithValue("@v50", request.v50);
                if (!String.IsNullOrEmpty(request.v51))
                    sqlCommand.Parameters.AddWithValue("@v51", request.v51);
                if (!String.IsNullOrEmpty(request.v52))
                    sqlCommand.Parameters.AddWithValue("@v52", request.v52);
                if (!String.IsNullOrEmpty(request.v53))
                    sqlCommand.Parameters.AddWithValue("@v53", request.v53);
                if (!String.IsNullOrEmpty(request.v54))
                    sqlCommand.Parameters.AddWithValue("@v54", request.v54);
                if (!String.IsNullOrEmpty(request.v55))
                    sqlCommand.Parameters.AddWithValue("@v55", request.v55);
                if (!String.IsNullOrEmpty(request.v56))
                    sqlCommand.Parameters.AddWithValue("@v56", request.v56);
                if (!String.IsNullOrEmpty(request.v57))
                    sqlCommand.Parameters.AddWithValue("@v57", request.v57);
                if (!String.IsNullOrEmpty(request.v58))
                    sqlCommand.Parameters.AddWithValue("@v58", request.v58);
                if (!String.IsNullOrEmpty(request.v59))
                    sqlCommand.Parameters.AddWithValue("@v59", request.v59);
                if (!String.IsNullOrEmpty(request.v60))
                    sqlCommand.Parameters.AddWithValue("@v60", request.v60);

                dataAdapter.Fill(result);

                sqlCommand.Dispose();
                sqlCommand.Connection.Close();
                dataAdapter.Dispose();

                return result;
            }
            catch (Exception e)
            {
                return new DataSet();
            }
        }
    }
}