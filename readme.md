# Project name
***
Intelligis - RTM - BackEnd

## General Info
***

RTM - BackEnd, 
Versión actual del componente: 3.0.2.7

## Technologies
***
Los componente están construidos sobre tecnología Microsoft .net, y compilados con Microsoft Visual Studio 2019.

Versiones del framewok: 4.5.2

## Prerequisites
***
Microsoft Visual Studio.Net 2019, con soporte para proyectos Web y framework 4

### General Info
***
1. Descargar el repositorio Git 
2. Abrir el proyecto por medio del archivo "com.intelligis.core.sln"
3. Compilar el proyecto principal "com.intelligis.core"
4. Verificar los archivos compilados según la configuración deseada (release/debug) en la carpeta Obj del proyecto

Configuración del archivo Web.config

<add key="ConnectionSQL" value="Data Source=rtmDBSrv,1433;Initial Catalog=rtmDB;Persist Security Info=True;User ID=rtm_DBUser;Password=123Password" />
<add key="File" value="C:\inetpub\wwwroot\rtmSrv\license\License.txt" />
<add key="PreActivate" value="C:\inetpub\wwwroot\rtmSrv\license\PreActivate.txt" />
<add key="urlPortal"  value="https://urlPortal/portal" />
<add key="dirLogs" value="C:\IG_RtmLogs\LogRtmServices" />
<add key="ConnectionTimeout" value="180" />
 
ConnectionSQL: Cadena de conexíon a base de datos
File: Ruta del archivo de licencia del componente RTM
PreActivate: Ruta del archivo de pre-activación del componente RTM
urlPortal: Ruta del servicio de acceso a Portal
dirLogs: Ruta de la carpeta donde se guardaran los Logs
ConnectionTimeout: Tiempo de espera del TimeOut

 